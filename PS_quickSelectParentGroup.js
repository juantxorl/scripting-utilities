//global variables
var curDoc = app.activeDocument;
var curLayer = curDoc.activeLayer;

function main(){
	if (curLayer.parent.typename == "LayerSet"){
		activeDocument.activeLayer = curLayer.parent;
	}

}

app.activeDocument.suspendHistory("SELECT PARENTGROUP", "main()");
