function main(){

    var curDoc = app.activeDocument;
    var curLayer = curDoc.activeLayer;


    function selectLayerPixels(){///// FIRST SELECTION FUNCION
        // =======================================================
        var id710 = charIDToTypeID( "setd" );
        var desc168 = new ActionDescriptor();
        var id711 = charIDToTypeID( "null" );
        var ref128 = new ActionReference();
        var id712 = charIDToTypeID( "Chnl" );
        var id713 = charIDToTypeID( "fsel" );
        ref128.putProperty( id712, id713 );
        desc168.putReference( id711, ref128 );
        var id714 = charIDToTypeID( "T   " );
        var ref129 = new ActionReference();
        var id715 = charIDToTypeID( "Chnl" );
        var id716 = charIDToTypeID( "Chnl" );
        var id717 = charIDToTypeID( "Trsp" );
        ref129.putEnumerated( id715, id716, id717 );
        desc168.putReference( id714, ref129 );
        executeAction( id710, desc168, DialogModes.NO );
    }

    function extendSelectLayerPixels(){///// EXTEND SELECTION FUNCION
        var idAdd = charIDToTypeID( "Add " );
            var desc294 = new ActionDescriptor();
            var idnull = charIDToTypeID( "null" );
                var ref18 = new ActionReference();
                var idChnl = charIDToTypeID( "Chnl" );
                var idChnl = charIDToTypeID( "Chnl" );
                var idTrsp = charIDToTypeID( "Trsp" );
                ref18.putEnumerated( idChnl, idChnl, idTrsp );
                var idLyr = charIDToTypeID( "Lyr " );
                //ref18.putName( idLyr, "Layer 2" );
            desc294.putReference( idnull, ref18 );
            var idT = charIDToTypeID( "T   " );
                var ref19 = new ActionReference();
                var idChnl = charIDToTypeID( "Chnl" );
                var idfsel = charIDToTypeID( "fsel" );
                ref19.putProperty( idChnl, idfsel );
            desc294.putReference( idT, ref19 );
            executeAction( idAdd, desc294, DialogModes.NO );

    }

    var newSelect = 0

    ///// LAYER PARSING
    function parseLayers(layerGroup, selectionStorage){
        if (layerGroup.typename == "LayerSet"){
            for(var i=0; i<layerGroup.layers.length;++i){
                if(layerGroup.layers[i].typename == "LayerSet"){//recursive includes subfolders
                    selectionStorage = parseLayers(layerGroup.layers[i], selectionStorage)
                }
                else if(layerGroup.layers[i].visible == true && layerGroup.layers[i].kind == 'LayerKind.NORMAL' && layerGroup.layers[i].grouped == false){
                    app.activeDocument.activeLayer = layerGroup.layers[i]
                    if(selectionStorage == 0){
                        selectionStorage = selectLayerPixels()
                    }else{
                        selectionStorage = extendSelectLayerPixels()
                    }
                }

                else{
                    continue
                }
            }
        }else{alert("please select a group and try again")}
        app.activeDocument.activeLayer = curLayer
        return selectionStorage
    }


    parseLayers(curLayer, newSelect)

};

app.activeDocument.suspendHistory("select group Contents", "main()");
