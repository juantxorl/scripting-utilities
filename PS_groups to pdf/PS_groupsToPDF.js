﻿function main() {


    ////////////////////////////////// ------------- \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\
    ////////////===================== USER INTERFACE =======================\\\\\\\\\\\
    ////////////////////////////////// ------------- \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\

    // CREATE USER INTERFACE
    var date = new Date()
    var month = date.getMonth()
    var year = date.getFullYear()
    var day = date.getDay()
    var fullDate = month + "/" + year

    w = new Window('dialog', 'GROUPS TO PDF 1.3', undefined);
    w.alignChildren = ['fill', 'fill']



    var optionsPanel = w.add('panel', undefined, 'SETTINGS',{borderStyle:'black'})//OPTIONS PANEL
    optionsPanel.margins = [15, 20, 15, 20]
    optionsPanel.alignChildren = ['fill', 'fill']


    var infoGroup = optionsPanel.add('group', undefined, undefined)//INFO GROUP
    infoGroup.alignChildren = ['fill', 'fill']
    infoGroup.margins = [0, 5, 0, 5]
    var regularExportOptions = infoGroup.add('group', undefined, undefined)

    var onlyPdfRB = regularExportOptions.add('RadioButton', undefined, "only PDF")
    onlyPdfRB.alignment = ['fill', 'fill']
    onlyPdfRB.value = true
    var onlyPngRB = regularExportOptions.add('RadioButton', undefined, "only JPGs")
    onlyPngRB.alignment = ['fill', 'fill']
    var PdfAndPngRB = regularExportOptions.add('RadioButton', undefined, "PDF + JPGs")
    PdfAndPngRB.alignment = ['fill', 'fill']
    var specialOptions = infoGroup.add('panel', undefined, '')
    specialOptions.alignment = ['right','fill']
    var pdfFromFilesCB = specialOptions.add('CheckBox', undefined, "PDF from files")
    pdfFromFilesCB.alignment = ['fill', 'fill']

    var optionsSubPanel1 = optionsPanel.add('group', undefined, undefined)
    optionsSubPanel1.orientation = 'column'
    optionsSubPanel1.alignChildren = ['left', 'fill']
    //optionsSubPanel1.margins = [0, 10, 0, 10]

    var optionsSubGroup = optionsSubPanel1.add('panel', undefined, 'Process Options')
    optionsSubGroup.alignment = 'fill'
    optionsSubGroup.orientation = 'row'
    var useJpegCB = optionsSubGroup.add('CheckBox', undefined, "jpgs(faster)")
    var includeCompleteImageCB = optionsSubGroup.add('CheckBox', undefined, "Include combined image")
    

    var include_Permafiles = optionsSubGroup.add('CheckBox', undefined, "include permafiles")
    include_Permafiles.value = true


    var overWritePdf = optionsSubGroup.add('CheckBox', undefined, "overWrite PDF")
    overWritePdf.value = true
    //overWritePdf.enabled = false
    overWritePdf.visible = false


    var bgcolorPanel = optionsSubPanel1.add('panel', undefined, "BG Page Color")//bg color for the pdf when transparency is found
    bgcolorPanel.alignment = 'fill'
    bgcolorPanel.orientation = 'row'
    bgcolorPanel.alignChildren = ['fill', 'fill']
    var useActivecolor = bgcolorPanel.add('RadioButton', undefined, "active color")
    var useWhite = bgcolorPanel.add('RadioButton', undefined, "white")
    useWhite.value = true
    var useBlack = bgcolorPanel.add('RadioButton', undefined, "black")
    var useTransparent = bgcolorPanel.add('RadioButton', undefined, "transparent")
    //useTransparent.enabled = false


    var boundsSubGroup = optionsSubPanel1.add('panel', undefined, 'Document Bounds')
    boundsSubGroup.orientation = 'row'
    boundsSubGroup.alignment = 'fill'
    var layerboundsRB = boundsSubGroup.add('RadioButton', undefined, 'clip to layer bounds')
    var documentSizeRB = boundsSubGroup.add('RadioButton', undefined, 'document size')
    documentSizeRB.value = true

    var pdfQualityPanel = optionsPanel.add('panel', undefined, 'PDF Options')
    pdfQualityPanel.orientation = 'row'

    dropDownName = pdfQualityPanel.add('StaticText', undefined, 'Quality:')
    var pdfQualityList = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12]
    var pdfQuality = pdfQualityPanel.add('DropDownList', undefined, pdfQualityList)
    pdfQuality.selection = 11
    pdfQuality.alignment = ['fill','fill']
    var showAlertsCB = pdfQualityPanel.add('CheckBox', undefined, "display pagination warnings")
    showAlertsCB.value = true
    showAlertsCB.alignment = ['right','fill']

    var postprocessGrp = optionsPanel.add('group', undefined, 'undefined')

    var openOutputFolder = postprocessGrp.add('CheckBox', undefined, "open destination folder")
    openOutputFolder.value = true
    var openPDF = postprocessGrp.add('CheckBox', undefined, "open PDF when finished")
    openPDF.value = false

    var storePrefsButton = postprocessGrp.add('button', undefined, "store user prefs")
    storePrefsButton.alignment = ['right','fill']
    //storePrefsButton.size = [100, 20]


    var executegrp = w.add('group', undefined, undefined)//EXECUTE/CANCEL PANEL
    executegrp.orientation = 'row'
    executegrp.alignChildren = ['fill','fill']

    var infoButton = executegrp.add('button', undefined, 'Info')
    infoButton.alignment = 'fill'
    var folderButton = executegrp.add('button', undefined, 'new/open folder')
    folderButton.alignment = 'fill'
    var closeButton = executegrp.add('button', undefined, "Cancel")
    closeButton.size = [120, 20]
    closeButton.alignment = ['center','fill']
    var executeButton = executegrp.add('button', undefined, "Go")
    executeButton.active = true
    executeButton.alignment = ['right','fill']
    executeButton.size = [180, 20]
    executeButton.graphics.foregroundColor = executeButton.graphics.newPen(executeButton.graphics.PenType.SOLID_COLOR, [1, 1, 1, 1], 1)
    executeButton.graphics.font = "Tahoma-Bold:12"

    

    ////////////////////////////////// info popup window \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\

    w2 = new Window('dialog', 'INFORMATION', undefined);
    w2.alignChildren = ['fill', 'fill']

    var tpanel = w2.add("tabbedpanel")
    tpanel.alignChildren = ['fill', 'fill']
    tpanel.preferredSize = [400,500]

    var t1 = tpanel.add('tab', undefined, "SPA")
    t1.alignChildren = ['fill','top']
    var infoPopupPanelSpa = t1.add('group', undefined, "INFO")//, { borderStyle: 'gray' })
    //infoPopupPanelSpa.alignment = ['fill', 'fill']
    infoPopupPanelSpa.orientation = 'row'
    infoPopupPanelSpa.alignChildren = ['fill','top']




    var infoTextSpa = infoPopupPanelSpa.add('StaticText', undefined, 'INSTRUCCIONES: \n\n Esta herramienta sirve para crear un pdf a partir de los grupos visibles que hay a nivel del documento. El proceso genera un jpg/png de cada grupo visible y luego los combina en un solo pdf. \n\n TEN EN CUENTA: \n\n 1. Solo se tienen en cuenta los grupos visibles de primer nivel. Cualquier capa o efecto no contenido dentro de ellos, se desecha. \n\n 2. El orden del pdf sigue el de los grupos del psd de arriba a abajo.\n\n 3. Los archivos de salida se encuentran en una subcarpeta con el nombre del archivo + "_processFolder" creada en la ruta del PSD. \n\n 4. Si se quiere simplemente generar un pdf con el material existente en la ruta de salida, se puede activar la casilla "PDF from files", que desactiva todas las opciones menos las relativas al PDF. \n (En caso de no haber exportado material anteriormente, se puede pulsar "create folder" para generar la carpeta de salida)', { multiline: true })
    //infoTextSpa.alignment = ['fill', 'fill']

    var panelLine1 = infoPopupPanelSpa.add('panel', undefined, 'OPCIONES')//, { borderStyle: 'black'})
    //panelLine1.alignment = ['fill', 'top']

    var infoTextSpa2 = panelLine1.add('StaticText', undefined, '\n\n FORMATOS \n\n Si se activa "only generate pngs", se mantienen los pngs en disco, pero no se crea el pdf.  \n\n Si se selecciona "pdf + pngs", se crean tanto pdf como pngs sin borrar nada.\n\n PERMAFILES \n\n Cualquier archivo adicional que se encuentre en la carpeta y que contenga un "#" en el nombre se considera un permaFile. Estos archivos no los borra la herramienta, y en el panel principal se puede activar la casilla de adjuntarlos al pdf si se desea. \n\n Para mantenerlo en el numero de pagina que uno desee, se debe incluir "XXX_#" en algún lugar del nombre del archivo (cambiando XXX por 001, 002, etc.). Ejemplos correctos serian: "001_#.png", "page002_#MyFile.jpg", "p003_#_my_file.psd". De lo contrario se adjuntan al final del documento.\n\n Al activar a casilla "also include combined image" se añade una hoja al inicio del pdf con la imagen completa del psd actualmente en uso.\n\n OTRAS \n\n Si se selecciona "only pdf" (por defecto), se crea el pdf y se desechan los png temporales.\n\n Desmarque "mostrar advertencias de paginado" si no desea recibir advertencias sobre el orden de las hojas con respecto a permafiles no numerados', {multiline: true})
    infoTextSpa2.alignment = ['fill', 'top']


    var t2 = tpanel.add('tab', undefined, "ENG")
    var infoPopupPanel = t2.add('group', undefined, 'INFO')//, { borderStyle: 'gray' })
    infoPopupPanel.alignment = ['fill', 'fill']
    infoPopupPanel.orientation = 'row'
    infoPopupPanel.alignChildren = ['fill','top']

    var infoTextEng = infoPopupPanel.add('StaticText', undefined, 'INSTRUCCIONS: \n\n This is a tool that generates a pdf with multiple pages, one from each top level group in the current document. the process creates a jpg/png of every group, and then combines them to a single pdf. \n\n KEEP THIS IN MIND: \n\n 1. Only the visible, top level groups will be used. Any clipped layers afecting them will be ignored \n\n 2. The pdf \'s page order will be the same as the psd\'s group, from top to bottom.\n\n 3. The output will be created in a subfolder of the current psd, that will be named "processFolder" \n\n 4. If you simply want to generate a PDF with the existing material in the output path, you can activate the "PDF from files" box, which deactivates all the options except those related to the PDF. \n (If you haven\'t previously exported material, you can click "create folder" to generate the output folder)', {multiline: true})
    infoTextEng.alignment = ['fill', 'fill']

    var panelLine3 = infoPopupPanel.add('panel', undefined, 'OPTIONS')
    panelLine3.alignment = ['fill', 'top']

    var infoTextEng2 = panelLine3.add('StaticText', undefined, 'EXPORT FORMATS \n\n If the default "only pdf" is selected, the pngs will be deleted at the end. \n\n If "only generate pngs" is enabled, it will skip the pdf creation but keep the pngs in disk.  \n\n If "pdf + pngs" is selected, pngs will be kept in disk, and the pdf will be created. \n\n PERMAFILES \n\n  Any additional files found in the folder that contain a "#" in the name are considered a permaFile. These files are not deleted by the tool, and in the main panel you can activate the checkBox to attach them to the PDF if desired. \n To keep it at the page number you want, you should name it "XXX_#", being "XXX" the page number. You can add more text to the filename before or after that, but the "XXX_#" must be somewhere in there, in that format (eg: "001_#.png", "page002_#MyFile.jpg", "p003_#_my_file.psd" are all valid names). Otherwise, they\'ll be added to the end of document.\n\n MISC \n\n The "also include combined image" will add a page to the pdf with the currently visible layers merged together. It will be the first page of the document \n\n Uncheck "display pagination warnings" if you don\'t want to get warnings about the ordering of the pages regarding permafiles without page number', {multiline: true})
    infoTextEng2.alignment = ['fill', 'top']
    var w2CloseButton = w2.add('Button', undefined, "OK")
    w2CloseButton.size = [30, 25]
    w2CloseButton.alignment = ['fill', 'bottom']

    var infoText3 = w2.add('StaticText', undefined, 'Groups to Pdf v1.3    Juan Gargallo    '  +  fullDate, { multiline: true })
    infoText3.alignment = ['fill', 'top']

    tpanel.selection = 0


    ///////////////////external files\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\
/*     var w3 = new Window('dialog', 'EXTERNAL FILES', undefined);
    w3.alignChildren = ['fill', 'fill']
    w3.preferredSize = [300,300]

    var w3t1 = w3.add('group', undefined, 'undefined')
    var ext_files_text = w3t1.add('StaticText', undefined, "now you can add some external files to the folder so they are included in the pdf. \n Be careful with the naming, because it will determine the position within the PDF", { multiline: true })
    ext_files_text.alignment = ['fill', 'fill']

    var open_window = w3t1.add('Button', undefined,"open destination folder")
    var w3CloseButton = w3.add('Button', undefined, "CONTINUE") */


    ////////////////////////////////// ------------- \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\
    ////////////====================== FUNCTIONALITY =======================\\\\\\\\\\\
    ////////////////////////////////// ------------- \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\


    //global variables
    var curDoc = app.activeDocument
    var curLayer = curDoc.activeLayer;
    var imageFilesList = []
    var permaFileList = []
    var user_prefs_folderPath = "~/AppData/Local/Adobe/GroupsToPdf/"
    var user_prefs_filePath = user_prefs_folderPath + "user_preferences.txt" 
    var user_lang = "spa"

    ///////////USER PREFERENCES
    var dict_user_prefs_txt = {};

    function storePrefs(input){
        //alert("funcion. Recursion: " + input.text + "  " + input.typeof)
        
        for (var i = 0; i < input.children.length; i++) {
            var currentElement = input.children[i];
            if (currentElement instanceof Panel ||  currentElement instanceof Group ) {
                storePrefs(currentElement)
            }
            if (currentElement instanceof RadioButton) {
                var curElement = currentElement.text
                //alert(currentElement + ",  " + curElement)
                dict_user_prefs_txt[curElement] = currentElement.value
                continue

            }
            else if (currentElement instanceof Checkbox){
                curElement = currentElement.text
                //alert(currentElement + ",  " + curElement)
                dict_user_prefs_txt[curElement] = currentElement.value

                continue

            }
            else if (currentElement instanceof DropDownList) {
                curElement = "pdfQuality"
                //alert(currentElement + ",  " + curElement)
                dict_user_prefs_txt[curElement] = currentElement.selection

                continue


            }

        }
        //alert("exiting: " + input)
        return
    }


    function writeDictionaryToFile(dictionary, filePath) {
        var file = new File(filePath);
        file.encoding = "UTF-8"; // Set file encoding (change if needed)
        file.open("w"); // Open file in write mode
    
        for (var key in dictionary) {
            if (dictionary.hasOwnProperty(key)) {
                var value = dictionary[key];
                file.writeln(key + ', ' + value); // Write key, value pair to file
            }
        }
    
        file.close(); // Close the file
    }
    
    ///////////////READ PREFERENCES

    function readPreferencesFile(filePath) {
        var preferences = {};
        var file = new File(filePath);
        file.encoding = "UTF-8"; // Set file encoding (should match the file's encoding)
        
        if (file.open("r")) {
            var line;
            while (!file.eof) {
                line = file.readln(); // Read line by line
    
                // Split each line into key and value using ','
                var keyValue = line.split(', ');

                var key = keyValue[0];
                var value = keyValue[1];
                preferences[key] = value; // Create key-value pair in the dictionary

            }
            file.close(); // Close the file
        }



        return preferences;
    }

    var parsedPreferences = readPreferencesFile(user_prefs_filePath)

    function outputPreferences(preferences) {
        for (var key in preferences) {
            if (preferences.hasOwnProperty(key)) {
                $.writeln(key + ": " + preferences[key]); // Output key-value pairs
            }
        }
    }

    $.writeln("Parsed Preferences:")
    outputPreferences(parsedPreferences) 
 
    /////////////// LOADING PREFERENCES INTO UI
    function ui_update_preferences(dict){
        //alert("onlyPDF RB from prefs file:  " + dict["only PDF"] + "   --  actual value: " + onlyPdfRB.value)

        if(dict["only PDF"] == "true"){
            onlyPdfRB.value = true
        }
        else if(dict["only JPGs"] == "true" || dict["only PNGs"] == "true"){
            onlyPngRB.value = true
        }
        else if(dict["PDF + JPGs"] == "true"||dict["PDF + PNGs"] == "true"){
            PdfAndPngRB.value = true
        }
        pdfFromFilesCB.value = (dict["PDF from files"] == "true")
        var jpegUse = dict["jpgs(faster)"]
        if(jpegUse == "true"){
            usePNGCheck(jpegUse)
            useTransparent.enabled = false
            useTransparent.text = "transparent(disabled)"
            useJpegCB.value = true
        }else{
            useJpegCB.value = false
        }

        includeCompleteImageCB.value = (dict["Include combined image"] == "true")
        include_Permafiles.value = (dict["include permafiles"] == "true")
        overWritePdf.value = (dict["overWrite PDF"] == "true")
        useActivecolor.value = (dict["active color"] == "true")
        useWhite.value = (dict["white"] == "true")
        useBlack.value = (dict["black"] == "true")
        useTransparent.value = (!jpegUse && dict["transparent(only png)"] == "true") || (dict["transparent"] == "true")
        layerboundsRB.value = (dict["clip to layer bounds"] == "true")
        documentSizeRB.value = (dict["document size"] == "true")
        pdfQuality.selection = dict["pdfQuality"]
        openOutputFolder.value = (dict["open destination folder"] == "true")
        openPDF.value = (dict["open PDF when finished"] == "true")
        if(pdfFromFilesCB.value){disable_regular_options()}

    }
    ui_update_preferences(parsedPreferences)
    ///////////////

    var workingDir = decodeURI(app.activeDocument.path)

    var includeComp = includeCompleteImageCB.value
    var onlyPNG = onlyPngRB.value
    var onlyPDF = onlyPdfRB.value
    var bothPDFAndPNG = PdfAndPngRB.value
    var openPDF_memory = 0
    ///////////----------------------- ORIG DOC NAME -----------------------\\\\\\\\\\\
    var originalDoc = app.activeDocument.name
    var originalDocName = originalDoc.split('.')[0]
    var tempFolderName = originalDoc.slice(0,-4)
    //originalDocName = originalDocName[0]
    var TempFolder = Folder(workingDir + '/' + tempFolderName + '_processFolder')

    ///////////----------------------- CUSTOM ALERTS -----------------------\\\\\\\\\\\


    function customAlert(message, delaySeconds, title){
        title = title || 'Alert';
        var alertWindow = new Window('palette', title);
        var control_text = alertWindow.add('statictext', undefined, message, {multiline: false});
        control_text.graphics.font = "Tahoma-Bold:12"
        control_text.graphics.foregroundColor = control_text.graphics.newPen(control_text.graphics.PenType.SOLID_COLOR, [1, 1, 1, 1], 1)
       
        if(delaySeconds == 0){
            var control_close = alertWindow.add('button', undefined, 'Close');       
            control_close.onClick = function(){
                if(alertWindow){
                    alertWindow.hide();
                }
            };
        }
    
        alertWindow.show();
        alertWindow.update();
       
        if(delaySeconds > 0){
            $.sleep(delaySeconds * 1000);
            alertWindow.hide();
            alertWindow = null;
        }  
    }


    ///////////----------------------- BG COLOR CHECK -----------------------\\\\\\\\\\\


    function colorCheck(){
        var colorvalue = []
        if (useActivecolor.value == true){colorvalue = [app.foregroundColor.rgb.red,app.foregroundColor.rgb.green,app.foregroundColor.rgb.blue]}
        else if(useWhite.value == true){colorvalue = [255,255,255]}
        else if(useBlack.value == true){colorvalue = [0,0,0]}
        else if(useTransparent.value == true){colorvalue = 'TRANSPARENT'}
        return colorvalue

    }

    ///////////----------------------- SAVE TO PNGS -----------------------\\\\\\\\\\\
    function createprocessFolder() {

        if (!TempFolder.exists) { TempFolder.create(); }

    }


    // FUNCIÓN DE GUARDADO DE LOS PNG
    function sfwPNG24(Name, counter,fileList) {
        var Name = decodeURI(app.activeDocument.name).replace(/\.[^\.]+$/, '');
        counter = counter.toString()
        counter = "0" + counter
        createprocessFolder()
        var saveFile = File(TempFolder + "/" + "page" + "0" + counter + "_" + Name + ".png");
        fileList.push(saveFile)

        var pngOpts = new PNGSaveOptions;
        pngOpts.compression = 9;
        pngOpts.interlaced = false;

        activeDocument.saveAs(saveFile, pngOpts, true, Extension.LOWERCASE);
        return fileList

    }

    function sfwJPG(Name, counter,fileList) {
        var Name = decodeURI(app.activeDocument.name).replace(/\.[^\.]+$/, '');
        
        counter = counter.toString()
        counter = "0" + counter
        createprocessFolder()
        var saveFile = File(TempFolder + "/" + "page" + "0" + counter + "_" + Name + ".jpg");
        fileList.push(saveFile)

        var jpgOpts = new JPEGSaveOptions();
        jpgOpts.embedColorProfile = true;
        jpgOpts.formatOptions = FormatOptions.STANDARDBASELINE;
        jpgOpts.matte = MatteType.NONE;
        jpgOpts.quality = 12;

        activeDocument.saveAs(saveFile, jpgOpts, true, Extension.LOWERCASE);
        return fileList
    }

    var docResolution = curDoc.resolution;


    function getLayers(){
        var curLayer = app.activeDocument.activeLayer;
        var workingDoc = app.documents.getByName(originalDoc)
        var groupList = []// ARRAY PARA ALMACENAR LA LISTA DE NOMBRES DE LOS GRUPOS QUE SE VAN A DUPLICAR, Y USARLOS LUEGO COMO NOMBRE DE CADA PNG
        for (i = 0; i < workingDoc.layers.length; ++i) {
            curLayer = workingDoc.layers[i]
            var getName = curLayer.name
            groupList.push(getName)
        }
        return groupList
    }

    function getValidLayers(){
        var curLayer = app.activeDocument.activeLayer;
        var workingDoc = app.documents.getByName(originalDoc)
        var groupList = []// ARRAY PARA ALMACENAR LA LISTA DE NOMBRES DE LOS GRUPOS QUE SE VAN A DUPLICAR, Y USARLOS LUEGO COMO NOMBRE DE CADA PNG
        for (i = 0; i < workingDoc.layers.length; ++i) {
            curLayer = workingDoc.layers[i]
            if(curLayer.typename == "LayerSet" && curLayer.visible == true){
                var getName = curLayer.name
                groupList.push(getName)
            }
    
        }
        return groupList
    }

    function duplicateLayerSets(layerboundsRB) { //PARSEO Y DUPLICADO DE LOS GRUPOS VISIBLES A DOCUMENTOS NUEVOS
        var curLayer = app.activeDocument.activeLayer;
        var workingDoc = app.documents.getByName(originalDoc)
        var groupList = []// ARRAY PARA ALMACENAR LA LISTA DE NOMBRES DE LOS GRUPOS QUE SE VAN A DUPLICAR, Y USARLOS LUEGO COMO NOMBRE DE CADA PNG
        var color = colorCheck()
        var useColor = 0
        var originalBgColor = app.backgroundColor //get original bg color for restoring it at the end of this function

        if(color != 'TRANSPARENT'){
            app.backgroundColor.rgb.red = color[0]
            app.backgroundColor.rgb.green = color[1]
            app.backgroundColor.rgb.blue = color[2]
            var useColor = 'BACKGROUNDCOLOR'
        }
        else{
            var useColor = 'DocumentFill.TRANSPARENT'
        }

        for (i = 0; i < workingDoc.layers.length; ++i) {
            var calcWidth = 0
            var calcHeight = 0
            curLayer = workingDoc.layers[i]

            if (curLayer.typename == "LayerSet" && curLayer.visible == true) {

                //// TOMA LOS BOUNDS DEL GRUPO. Falta apañar las capas de ajuste               
                if (layerboundsRB == true) {

                    calcWidth = workingDoc.layers[i].boundsNoEffects[2] - workingDoc.layers[i].boundsNoEffects[0];
                    calcHeight = workingDoc.layers[i].boundsNoEffects[3] - workingDoc.layers[i].boundsNoEffects[1];

                    var getName = curLayer.name
                    groupList.push(getName)
                    var doc2 = app.documents.add(calcWidth, calcHeight, docResolution, getName)

                    if(color == 'TRANSPARENT'){
                        doc2.layers[0].isBackgroundLayer = false
                        doc2.layers[0].visible = false
                    }
                    else if(color != 'TRANSPARENT'){
                        doc2.selection.selectAll()
                        doc2.selection.fill(app.backgroundColor)
                    }

                    app.activeDocument = workingDoc
                    curLayer.duplicate(doc2)


                    //pone en foco el nuevo documento y colapsa el grupo recién creado para poder recortarlo a los bordes del grupo de manera correcta

                    var newDoc = app.documents.getByName(getName)
                    app.activeDocument = newDoc
                    var curLayerSet = newDoc.layerSets.getByName(getName)
                    curLayerSet.merge()

                    var bounds = [newDoc.layers[0].boundsNoEffects[0], newDoc.layers[0].boundsNoEffects[1], newDoc.layers[0].boundsNoEffects[2], newDoc.layers[0].boundsNoEffects[3]]
                    newDoc.crop(bounds)

                    app.activeDocument = workingDoc

                    //// TOMA LOS LÍMITES DEL DOCUMENTO
                }

                else if (layerboundsRB == false) {

                    workingDoc.selection.selectAll()
                    calcWidth = workingDoc.selection.bounds[2] - workingDoc.selection.bounds[0];
                    calcHeight = workingDoc.selection.bounds[3] - workingDoc.selection.bounds[1];
                    getName = curLayer.name
                    

                    groupList.push(getName)
                    doc2 = app.documents.add(calcWidth, calcHeight, docResolution, getName)
                    if(color == 'TRANSPARENT'){
                        doc2.layers[0].isBackgroundLayer = false
                        doc2.layers[0].visible = false
                    }
                    else if(color != 'TRANSPARENT'){
                        doc2.selection.selectAll()
                        doc2.selection.fill(app.backgroundColor)
                    }
                    app.activeDocument = workingDoc
                    curLayer.duplicate(doc2)
                }




            }
        }
        app.backgroundColor = originalBgColor
        return groupList

    }


    ///////////-----------------------              -----------------------\\\\\\\\\\\
    ///////////----------------------- CREATE PDFS  -----------------------\\\\\\\\\\\
    ///////////-----------------------              -----------------------\\\\\\\\\\\


    //////////---------------CHECK IF PDF IS ALREADY OPEN

    function pdf_is_open(path){

        var file = new File(path);
        var isOpenInAnotherApp = false;

        file.remove(); // Delete the file
        if(file.exists){
            isOpenInAnotherApp = true;
        }

        return isOpenInAnotherApp
    }


    function pdfSaveOptions(pdfQualitySet, openPDFSet) {//OPCIONES DE GUARDADO DEL PDF
        if(openPDF.enabled == false){
            openPDFSet = false
        }
        var o = new PDFSaveOptions();
        var quality = pdfQualitySet
        o.PDFCompatibility = PDFCompatibility.PDF17;

        o.PDFStandard = PDFStandard.NONE;

        o.colorConversion = true; // convert to destinationProfile
        o.description = ""; // description of the save options in use
        o.destinationProfile = "sRGB IEC61966-2.1";

        o.downSample = PDFResample.PDFBICUBIC;

        o.downSampleSize = 150; // pixels per inch desired
        o.downSampleSizeLimit = 150; // only images pixels per inch greater than this value
        o.embedColorProfile = false;
        o.embedThumbnail = true;

        o.encoding = PDFEncoding.JPEG;
        o.jpegQuality = pdfQualitySet; // range 0 to 12
        o.optimizeForWeb = true;


        // These properties set either way.
        o.alphaChannels = true;
        o.annotations = false;
        o.convertToEightBit = true;
        o.layers = false;
        o.preserveEditing = false; // true retains Photoshop data
        o.spotColors = false;
        o.view = openPDFSet; // true opens PDF in Acrobat after saving it
        return o;
    }

    function presentationOptions(pdfQualitySet, openPDFSet) {//ESTO SE PODRÍA ELIMINAR PORQUE NUNCA VAMOS A QUERER UNA PRESENTACIÓN, PERO HABRÍA QUE SACAR LA FUNCIÓN DE PDFSAVEOPTIONS DE AHÍ DENTRO PARA QUE PUEDA SER LLAMADA
        var o = new PresentationOptions();
        o.PDFFileOptions = pdfSaveOptions(pdfQualitySet, openPDFSet);

        o.autoAdvance = false;
        o.includeFilename = false;
        o.interval = 5;
        o.loop = false;

        o.magnification = MagnificationType.FITPAGE;

        o.transistion = TransitionType.NONE;

        return o;
    }


    function getFiles(folder) {
        var files = folder.getFiles()
        var results = []
        for (i = 0; i < files.length; i++) {
            f = files[i]
            results.push(f)
        }
        return results
    }

    function checkExtension(file) {
        var name = file.name.split(".")
        name = name[1]
        return name
    }

    function setPdfName(outputfolder,PDF_name,pdf_found) {
        try {
            if(overWritePdf.value == false){
                var newName = PDF_name.split(".")[0]
                //PDF_name = newNAme[0]
                alert(newName)
                var outputFile = File(outputfolder + "/" + newName + "_" + "copy" + ".pdf")
                return outputFile
            }
            else if(overWritePdf.value == true || pdf_found == true){
                /* var numberingList = []
                numerator = "1" */
                var outputFile = File(outputfolder + "/" + originalDocName + "_" + "export" + ".pdf")
                return outputFile
            }


        } catch (error) { alert(error) }

    }

    function permaFile(name,target){
        return name.indexOf(target) >= 0
    }

    function deleteFiles(outputfolder, ext) {
        var files = getFiles(outputfolder);
    
        if (include_Permafiles.value == true) {
            for each(var isFile in files) {
                extension = checkExtension(isFile)
                var isPermafile = permaFile(isFile.name,"#")
                if (extension == ext && isPermafile == false) {
                    isFile.remove()
                }
            }
        }
        else
        {
            for each(var isFile in files) {
                extension = checkExtension(isFile)
                var isPermafile = permaFile(isFile.name,"#")
                if (extension == ext && isPermafile == false) {
                    isFile.remove()
                }
            }
        }

    }

    function permafile_populate(outputfolder){
        var files = getFiles(outputfolder);
        for each(var isFile in files){
            if (permaFile(isFile.name,"#") == true){
                permaFileList.push(isFile)
            }
        }
    }

    function sort_permafiles_in_list(permafile_list, files_list) {
        var permaFiles_error_array = ""
        for (var i = 0; i < permafile_list.length; ++i) {
            var fileName = permafile_list[i].name
            var match = fileName.match(/(\d+)_#/);
            if (match) {
                var extractedNumber = parseInt(match[1],10) - 1;
                //var extractedNumber = parseInt(extractedNumberString, 10);
                files_list.splice(extractedNumber, 0, permafile_list[i]);
                //alert(extractedNumber)
            } else {
                if (permaFiles_error_array == ""){
                    permaFiles_error_array = permafile_list[i].name
                }
                else{
                    permaFiles_error_array = permaFiles_error_array + "\n\n " + permafile_list[i].name
                }
                var extractedNumber = 900
                files_list.splice(extractedNumber, 0, permafile_list[i]);
                // Handle cases where the filename doesn't match the expected pattern
            }
        }
        if (permaFiles_error_array != "" && showAlertsCB && !onlyPngRB.value ){
            alert("the following files don't have a page number, and will be added at the end of pdf:\n\n " + permaFiles_error_array + "\n\n To prevent this, remember to add the scheme \"pagenumber_#\" (eg: 001_#) somewhere in the name","PAGINATION ISSUE", {multiline: true}); 
        }
        return files_list;
    }

    function processPDF(outputfolder, pdfQualitySet, openPDFSet, fileList,cur_PDF_name,pdf_found) {
        var files = fileList
        app.displayDialogs = DialogModes.NO;

        var newFile = setPdfName(outputfolder,cur_PDF_name,pdf_found)
        if (!files.length) {
            doneMessage = "No files found in selected folder";
            return;
        } 

        else {
            // Make presentation (multi-page PDF of the images)
            app.makePDFPresentation(files, newFile, presentationOptions(pdfQualitySet, openPDFSet));
        }

    }
    function checkLayerFormatting(layerList){
        //var pattern = /[^\w,.\s()+-]/g
        var formatOk = 1
        for(i=0; i<layerList.length;++i){
            var name = layerList[i]
            var matches = name.match(/[^\w,.\s()+^-]/g);

            if(matches == null){continue}
            else if (matches){
                formatOk = 0
                alert("NAMING ISSUE!! \n\n Unsupported characters used:    " + matches + " \n\n in this layer:    " + name + "\n\n\n Only the following special characters are permitted: + - . , ( ) \n Please reformat your layers and try again.")

            }

        }

        return formatOk
    }

    function openFolderInExplorer(outputfolder) {
        outputfolder.execute();
    }
    ///////////-----------------------              -----------------------\\\\\\\\\\\
    ///////////----------------------- PROCESS DOCS -----------------------\\\\\\\\\\\
    ///////////-----------------------              -----------------------\\\\\\\\\\\
    function execution(outputfolder, layerboundsRB, openOutput) {
        var files = getFiles(outputfolder);
        onlyPDF = onlyPdfRB.value
        onlyPNG = onlyPngRB.value
        bothPDFAndPNG = PdfAndPngRB.value

        var layerNames = getLayers()
       
        var formatOk = checkLayerFormatting(layerNames)

        if(formatOk == 0){
            w.close()
            return}
        if(pdfFromFilesCB.value == false){
            deleteFiles(outputfolder, "png")
            deleteFiles(outputfolder, "jpg")
            if(overWritePdf.value == true){
                deleteFiles(outputfolder, "pdf")
            }
        }
        
        var check_If_Pdf_Open = getFiles(outputfolder);
        if (check_If_Pdf_Open.length > 0) {
            for each(var isFile in check_If_Pdf_Open){
                extension = checkExtension(isFile)
                if (extension == "pdf"){
                    var cur_PDF_name = isFile.name
                    var pdf_found = true
                }
                else{
                    pdf_found = false
                }

            }
            
        }

        if(pdfFromFilesCB.value == true){ // generate the pdf with the files that are in the output folder. 
                                          //Useful for when you made some mistake, or want something more edge-case
            deleteFiles(outputfolder, "pdf")
            files = getFiles(outputfolder)
            if(files.length == 0){
                customAlert('NO FILES FOUND. CANCELLING', 1, 'ALERT')
                return
            }
            processPDF(outputfolder, pdfQualitySet, openPDF.value,files,cur_PDF_name,pdf_found)
            w.close()
            openFolderInExplorer(outputfolder)
            return
        }

        app.displayDialogs = DialogModes.NO;
        var list = duplicateLayerSets(layerboundsRB)

        
        var counter = 0
        //alert(imageFilesList)

        for (i = 0; i < list.length; ++i) {
            var docName = list[i]
            if (!docName) {
                continue
            }
            else {
                counter += 1
                app.activeDocument = app.documents.getByName(docName)
                if (useJpegCB.value) {
                    fileList = sfwJPG(docName, counter,imageFilesList)
                } else {
                    fileList = sfwPNG24(docName, counter,imageFilesList)

                }

                app.activeDocument.close(SaveOptions.DONOTSAVECHANGES);

            }

        }

        //alert("images exported!, press ok for continue with pdf")
        if (includeComp == true) {// si el checkbox de la imagen completa está activado, crea un png también de la primera imagen
            app.activeDocument = app.documents.getByName(originalDoc)
            if (useJpegCB.value) {
                fileList = sfwJPG(originalDoc, 0, imageFilesList)
            } else {
                fileList = sfwPNG24(originalDoc, 0, imageFilesList)
            }
        }


        //add external files to file List
        permafile_populate(outputfolder)
        if(include_Permafiles.value){
            fileList = sort_permafiles_in_list(permaFileList, fileList)//insert permafiles
            
        }


        if (onlyPDF == true) {
            processPDF(outputfolder, pdfQualitySet, openPDF.value,fileList,cur_PDF_name,pdf_found)
            deleteFiles(outputfolder, "png")
            deleteFiles(outputfolder, "jpg")

        } else if (bothPDFAndPNG == true) {
            processPDF(outputfolder, pdfQualitySet, openPDF.value,fileList,cur_PDF_name,pdf_found)
        }
        
        app.activeDocument.selection.deselect()
        if (openOutput == true) {
            openFolderInExplorer(TempFolder)
        }
        //collapse all groups
        var idcollapseAllGroupsEvent = stringIDToTypeID("collapseAllGroupsEvent");
        var desc = new ActionDescriptor();
        executeAction(idcollapseAllGroupsEvent, desc, DialogModes.NO);
        return
    }

 

    ////////////////////////////////// ------------- \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\
    ////////////===================== EVENT HANDLING =======================\\\\\\\\\\\
    ////////////////////////////////// ------------- \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\

    if(!TempFolder.exists) {
        folderButton.text = 'create folder'
    }else{
        folderButton.text = 'open folder'
    }

    w.addEventListener ("keydown", function(event){
        if (event.keyName == 'Enter') {
            execution(TempFolder, layerboundsRB.value, openOutputFolder.value)
            w.close()
            app.activeDocument.activeHistoryState = doc.historyStates[doc.historyStates.length - 2]
        }
    })

    


    function checkText(checkJpg) {
        if (onlyPdfRB.value == true) {
            executeButton.text = "Create only PDF"
        }

        if (checkJpg == true) {

            if (onlyPngRB.value == true) {
                executeButton.text = "Create only JPGs"
            }
            else if (PdfAndPngRB.value == true) {
                executeButton.text = "Create PDF + JPGs"
            }
        } else if (checkJpg == false) {
            if (onlyPngRB.value == true) {
                executeButton.text = "Create only PNGs"
            }
            else {
                executeButton.text = "Create PDF + PNGs"
            }
        }


        if (checkJpg) {
            PdfAndPngRB.text = "PDF + JPGs"
            onlyPngRB.text = "only JPGs"
            useTransparent.enabled = false
            useTransparent.text = "transparent(disabled)"
            if(useTransparent.value){
                useWhite.value = true
            }
        } else {
            PdfAndPngRB.text = "PDF + PNGs"
            onlyPngRB.text = "only PNGs"
            useTransparent.enabled = true
            useTransparent.text = "transparent"
        }


    }


    storePrefsButton.onClick = function(){
       
        var prefsFolder = new Folder(user_prefs_folderPath);
        if (!prefsFolder.exists) { // Check if the folder exists
        
            var created = prefsFolder.create();// If the folder doesn't exist, create it
            
            if (!created) {// Handle if folder creation fails
                alert("Failed to create the application folder.");
            }
        }

    
        storePrefs(w)

        writeDictionaryToFile(dict_user_prefs_txt, user_prefs_filePath)

    }

    function disable_regular_options(){
        if(pdfFromFilesCB.value){
            regularExportOptions.enabled = false
            optionsSubPanel1.enabled = false
        }
        else{
            regularExportOptions.enabled = true
            optionsSubPanel1.enabled = true
        }
    }

    pdfFromFilesCB.onClick = function (){disable_regular_options()}

    //open_window.onclick = function(){openFolderInExplorer(TempFolder)}
    
    overWritePdf.onClick = function(){
        return overWritePdf.value
    }

    include_Permafiles.onClick = function(){
        return include_Permafiles.value
    }

    infoButton.onClick = function () {
        w2.preferredSize = [450, 450]
        w2.center()
        w2.show()
        infoButton.enabled = false

    }

    var pdfQualitySet = 10
    var openPDFSet = false



    includeCompleteImageCB.onClick = function () {
        includeComp = includeCompleteImageCB.value
        return includeComp
    }

    function usePNGCheck(isjpeg){
        if(isjpeg == true){
            useTransparent.enabled = false
            useTransparent.text = "transparent(disabled)"
            if(useTransparent.value){
                useWhite.value = true
            }
        }
        else{
            useTransparent.enabled = true
            useTransparent.text = "transparent"
        }
        return isjpeg
    }

    useJpegCB.onClick = function(){
        checkText(useJpegCB.value)
        return useJpegCB.value
    }



    //COMPRUEBA SI ESTÁ ACTIVADA LA OPCIÓN DE SÓLO JPG/PNG
    infoGroup.addEventListener('click', radioCheck)

    function radioCheck() {
        //alert(original_PdfState)
        if (onlyPngRB.value == true) {
            //openPDF.value = false
            openPDF.enabled = false
            include_Permafiles.visible = false
        } else {
            include_Permafiles.visible = true
            openPDF.enabled = true
        }
    }

    onlyPngRB.onClick = function () {
        //radioCheck()
        checkText(useJpegCB.value)
        return
    }
    
    PdfAndPngRB.onClick = function () {
        //radioCheck()
        checkText(useJpegCB.value)
        return
    }

    onlyPdfRB.onClick = function () {
        //radioCheck()
        checkText(useJpegCB.value)

        return
    }
/*
     w.onDraw = function(){
        checkText(useJpegCB.value)
        return
    } */

    pdfQuality.onChange = function () {
        pdfQualitySet = this.selection
        pdfQualitySet = parseInt(pdfQualitySet) + 1
        return pdfQualitySet
    }

/*     openPDF.onClick = function () {
        return openPDF.value
    } */

    documentSizeRB.onClick = function () {
        documentSizeRB.value = true
        layerboundsRB.value = false
    }

    layerboundsRB.onClick = function () {
        documentSizeRB.value = false
        layerboundsRB.value = true
    }

    openOutputFolder.onClick = function () {
        return openOutputFolder.value
    }
    folderButton.onClick = function () {
        createprocessFolder()
        openFolderInExplorer(TempFolder)
        folderButton.text = 'open folder'
    }

    executeButton.onClick = function () {
        if(onlyPdfRB.value || PdfAndPngRB.value){
            var pdfName = TempFolder + "/" + originalDocName + "_export.pdf"
            var pdf_in_use = pdf_is_open(pdfName)
            if(pdf_in_use){
                alert("pdf is already open. Please close it before proceeding")
                return
            }else{
                execution(TempFolder, layerboundsRB.value, openOutputFolder.value)
                w.close()
                //app.scriptPreferences.properties = sp;
                app.activeDocument.activeHistoryState = doc.historyStates[doc.historyStates.length - 2]
            }
        }
        else{
            execution(TempFolder, layerboundsRB.value, openOutputFolder.value)
            w.close()
            //app.scriptPreferences.properties = sp;
            app.activeDocument.activeHistoryState = doc.historyStates[doc.historyStates.length - 2]
        }
    }

    closeButton.onClick = function () { w.close() }

    w2CloseButton.onClick = function () {
        w2.close()
    }

    w.center()
    w.show()

}

app.activeDocument.suspendHistory("foldersToDocs", "main()");