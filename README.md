Hello!
This is a collection of very simple scripts for Photoshop. They are mainly focused on easing certain tasks within the layer stack:

1. Relocate: Useful for moving a layer or group within a large layer stack, ignoring the tedious click and drag
2. Batch rename: self explanatory
3. Applying adjustments to individual layers within a group. Sometimes a group has a couple of adjusment layers above it, affecting only that group, and you may want to apply that adjustments to the layers within, to be able to delete and clean your workflow.
    This script eases it.
    
    Feel free to use it and/or improve upon it. Just be kind, and give credit when it is due! ;)